package com.android.recipe;

import com.android.recipe.activity.MainActivity;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MainActivityUntiTest {
    @Test
    public void islastpage_returnstrue() throws Exception {

        MainActivity mainActivity=new MainActivity();

        boolean result=mainActivity.isLastPage(25);
        assertEquals(true, result);
    }
    @Test
    public void islastpage_returnsfalse() throws Exception {

        MainActivity mainActivity=new MainActivity();

        boolean result=mainActivity.isLastPage(35);
        assertEquals(false, result);
    }
}