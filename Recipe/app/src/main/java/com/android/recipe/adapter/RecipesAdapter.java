package com.android.recipe.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.recipe.R;
import com.android.recipe.activity.RecipeDetails;
import com.android.recipe.model.RecipesModel;
import com.android.recipe.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Farhan on 11/14/2017.
 */

public class RecipesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    private ArrayList<RecipesModel> mRecipes;
    private static Context mContext;

    public RecipesAdapter() {
        mRecipes = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view = inflater.inflate(R.layout.list_item_recipe, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((ViewHolder) holder).bind(mRecipes.get(position));

    }

    @Override
    public int getItemCount() {
        return mRecipes == null ? 0 : mRecipes.size();
    }


    /**
     * Helpers
     */

    public void add(RecipesModel fact) {
        mRecipes.add(fact);
        notifyItemInserted(mRecipes.size() - 1);
    }

    public void addAll(List<RecipesModel> mcList) {
        for (RecipesModel mc : mcList) {
            add(mc);
        }
    }

    public void remove(RecipesModel fact) {
        int position = mRecipes.indexOf(fact);
        if (position > -1) {
            mRecipes.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public RecipesModel getItem(int position) {
        return mRecipes.get(position);
    }


    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView;
        private ImageView recipeImage;
        private LinearLayout itemLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.title_text);
            recipeImage = (ImageView) itemView.findViewById(R.id.recipe_image);
            itemLayout=(LinearLayout)itemView.findViewById(R.id.item_layout);
        }

        public void bind(RecipesModel recipe) {

            titleTextView.setText(recipe.getTitle());
            final String titleText= recipe.getTitle();
            final String rId=recipe.getRecipeId();
            Picasso.with(mContext).load(recipe.getImageUrl()).placeholder(R.drawable.placeholder).into(recipeImage);

            itemLayout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, RecipeDetails.class);
                    intent.putExtra(Constant.RECIPE_TITLE, titleText);
                    intent.putExtra(Constant.RECIPE_ID, rId);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
