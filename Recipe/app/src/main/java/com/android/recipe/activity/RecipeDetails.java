package com.android.recipe.activity;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.recipe.R;
import com.android.recipe.api.ApiClient;
import com.android.recipe.api.ApiInterface;
import com.android.recipe.api.CallbacksManager;
import com.android.recipe.model.RecipeDetailModel;
import com.android.recipe.model.RecipeDetailResponseModel;
import com.android.recipe.utils.Constant;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;

import retrofit2.Call;
import retrofit2.Response;

public class RecipeDetails extends BaseActivity {

    private CallbacksManager mCallbacksManager = new CallbacksManager();

    private ImageView recipeImageBig;
    private LinearLayout ingredientsLayout;
    private TextView instructionsSourceTextView;
    private TextView originalSourceTextView;
    private TextView publisherName;
    private TextView socialRank;

    private RecipeDetailModel recipeDetailModel=null;

    private Context mContext;


    private String recipeTitle;
    private String recipeId;

    @Override
    protected int setActivityLayout() {
        return R.layout.activity_recipe_details;
    }

    @Override
    protected void initViews() {

        mContext=this;
        recipeImageBig=(ImageView) findViewById(R.id.recipe_image_big);
        ingredientsLayout=(LinearLayout) findViewById(R.id.ingredients_layout);
        instructionsSourceTextView=(TextView) findViewById(R.id.instructions_textview);
        originalSourceTextView=(TextView) findViewById(R.id.original_textview);
        publisherName=(TextView) findViewById(R.id.publisher_name);
        socialRank=(TextView) findViewById(R.id.social_rank);
    }

    @Override
    protected void initValues() {

        Intent intent=getIntent();
        recipeTitle=intent.getStringExtra(Constant.RECIPE_TITLE);
        recipeId=intent.getStringExtra(Constant.RECIPE_ID);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(recipeTitle);
    }

    @Override
    protected void initValuesInViews() {
        getRecipe(recipeId);
    }

    @Override
    protected void setListenersOnViews() {

        //Loading webviews with the user selection

        instructionsSourceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recipeDetailModel!=null)
                    new FinestWebView.Builder(mContext).show(recipeDetailModel.getF2fUrl());
            }
        });
        originalSourceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recipeDetailModel!=null)
                    new FinestWebView.Builder(mContext).show(recipeDetailModel.getSourceUrl());
            }
        });
    }


    //Handling back button in actionbar properly
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Function to receive recipe details from server
    private void getRecipe(String recipeId)
    {
        CallbacksManager.CancelableCallback recipeRequest = mCallbacksManager.new CancelableCallback(null) {

            //Handling of network response
            @Override
            protected void response(Response response, View mRecycleView) {

                RecipeDetailResponseModel responseModel = (RecipeDetailResponseModel) response.body();
                RecipeDetailModel recipe=responseModel.getRecipe();
                Picasso.with(mContext).load(recipe.getImageUrl()).into(recipeImageBig);

                setIngredients(recipe.getIngredients());

                publisherName.setText(recipe.getPublisherName());
                //rounding rank to 2 decimal places so that it should fit properply while displaying
                String rank = String.format("%.2f", recipe.getSocialRank());
                socialRank.setText("Social Rank: "+rank);

                recipeDetailModel=recipe;
            }

            @Override
            protected void failure(Response response, Throwable error) {
                Toast.makeText(RecipeDetails.this, "error", Toast.LENGTH_LONG).show();
            }
        };

        ApiInterface apiService = ApiClient.getApiService();
        Call<RecipeDetailResponseModel> log = apiService.getRecipe("b549c4c96152e677eb90de4604ca61a2",recipeId);
        log.enqueue(recipeRequest);

    }

    //function to set ingredients to the view by creating dynamic textviews in a linear layout
    private void setIngredients(String [] ingredients)
    {
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        for(int i=0;i<ingredients.length;i++) {
            TextView tv = new TextView(mContext);
            tv.setLayoutParams(lparams);
            tv.setText("-- "+ingredients[i]);
            ingredientsLayout.addView(tv);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCallbacksManager.pauseAll();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCallbacksManager.resumeAll();
    }

    @Override
    public void onDestroy() {
        mCallbacksManager.cancelAll();
        super.onDestroy();
    }

}
