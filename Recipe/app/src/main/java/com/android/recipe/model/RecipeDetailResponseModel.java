package com.android.recipe.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 11/16/2017.
 */

public class RecipeDetailResponseModel {

    @SerializedName("recipe")
    private RecipeDetailModel recipe;

    public RecipeDetailModel getRecipe() {
        return recipe;
    }

    public void setRecipe(RecipeDetailModel recipe) {
        this.recipe = recipe;
    }

    public RecipeDetailResponseModel() {
    }
}
