package com.android.recipe.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 11/14/2017.
 */

public class RecipeDetailModel {

        @SerializedName("publisher")
        private String publisherName;
        @SerializedName("f2f_url")
        private String f2fUrl;
        @SerializedName("ingredients")
        private String [] ingredients;
        @SerializedName("source_url")
        private String sourceUrl;
        @SerializedName("image_url")
        private String imageUrl;
        @SerializedName("social_rank")
        private double socialRank;

    public RecipeDetailModel() {
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getF2fUrl() {
        return f2fUrl;
    }

    public void setF2fUrl(String f2fUrl) {
        this.f2fUrl = f2fUrl;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(String[] ingredients) {
        this.ingredients = ingredients;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getSocialRank() {
        return socialRank;
    }

    public void setSocialRank(double socialRank) {
        this.socialRank = socialRank;
    }
}
