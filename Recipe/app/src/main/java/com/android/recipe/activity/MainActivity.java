package com.android.recipe.activity;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.recipe.R;
import com.android.recipe.adapter.RecipesAdapter;
import com.android.recipe.api.ApiClient;
import com.android.recipe.api.ApiInterface;
import com.android.recipe.api.CallbacksManager;
import com.android.recipe.listener.PaginationScrollListener;
import com.android.recipe.model.RecipesResponseModel;

import retrofit2.Call;
import retrofit2.Response;


/**
 * Created by Farhan on 11/14/2017.
 */

public class MainActivity extends BaseActivity {



    private Context mContext;
    RecipesResponseModel responseModel=null;
    private CallbacksManager mCallbacksManager = new CallbacksManager();

    private RecyclerView mRecyclerView;
    private LinearLayoutManager layoutManager;
    private RecipesAdapter mAdapter;
    private ProgressBar mProgressBar;
    private SearchView mSearchView;

    private boolean isLoading = false;
    private boolean lastPage = false;

    private String searchString;

    private static final int PER_PAGE_RECIPES = 30;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    @Override
    protected int setActivityLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        mProgressBar = findViewById(R.id.progress_bar);
        mRecyclerView = findViewById(R.id.facts_recycler_view);
        mSearchView = findViewById(R.id.search_view);
    }

    @Override
    protected void initValues() {
        mAdapter = new RecipesAdapter();
        layoutManager = new LinearLayoutManager(this);
        mContext=this;
    }

    @Override
    protected void initValuesInViews() {
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void setListenersOnViews() {

        //Scroll listener to check if there is a need to load next page
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                Log.e("recyclerview", "loadMoreItems");
                isLoading = true;
                currentPage += 1;
                mProgressBar.setVisibility(View.VISIBLE);
                getRecipes(currentPage, false);
            }

            @Override
            public boolean isLastPage() {
                return lastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        //Query listener for searchview
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                //Handling new query and getting new recipes based on new search query

                mAdapter.clear();
                if(query.isEmpty() || (query.trim()).isEmpty())
                {
                    return false;
                }
                searchString=query;
                isLoading = false;
                lastPage = false;

                mProgressBar.setVisibility(View.VISIBLE);
                getRecipes(PAGE_START, true);
                currentPage=PAGE_START;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void getRecipes(int pageNumber, final boolean firstRunCheck) {
        if (firstRunCheck)
            mProgressBar.setVisibility(View.VISIBLE);
        CallbacksManager.CancelableCallback recipesRequest = mCallbacksManager.new CancelableCallback(null) {
            @Override
            protected void response(Response response, View mRecycleView) {
                responseModel = (RecipesResponseModel) response.body();

                //adding recipes to the list
                mAdapter.addAll(responseModel.getRecipes());

                if (!firstRunCheck) {
                    Log.d("Pagination", "loadMorePage: ");
                    mProgressBar.setVisibility(View.GONE);
                    isLoading = false;
                    if (isLastPage(responseModel.getCount()))
                        lastPage = true;
                    else
                        mProgressBar.setVisibility(View.GONE);

                } else {
                    mProgressBar.setVisibility(View.GONE);
                    Log.d("Pagination", "loadFirstPage: ");
                    if (isLastPage(responseModel.getCount()))
                        lastPage = true;
                    else
                        mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            protected void failure(Response response, Throwable error) {
                Toast.makeText(MainActivity.this, "error", Toast.LENGTH_LONG).show();
            }
        };


        //Adding network call
        ApiInterface apiService = ApiClient.getApiService();
        Call<RecipesResponseModel> log = apiService.getRecipes("b549c4c96152e677eb90de4604ca61a2",searchString,pageNumber);
        log.enqueue(recipesRequest);

    }

    public boolean isLastPage(long recipeCount)
    {
        if (recipeCount<PER_PAGE_RECIPES)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCallbacksManager.pauseAll();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCallbacksManager.resumeAll();
    }

    @Override
    public void onDestroy() {
        mCallbacksManager.cancelAll();
        super.onDestroy();
    }
}
