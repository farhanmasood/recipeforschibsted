package com.android.recipe.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Farhan on 11/14/2017.
 */

public class RecipesResponseModel {

    @SerializedName("count")
    private long count;

    @SerializedName("recipes")
    private ArrayList<RecipesModel> recipes;

    public long getCount() {
        return count;
    }

    public void setCount(long total) {
        this.count = total;
    }

    public ArrayList<RecipesModel> getRecipes() {
        return recipes;
    }

    public void setRecipes(ArrayList<RecipesModel> recipes) {
        this.recipes = recipes;
    }
}
