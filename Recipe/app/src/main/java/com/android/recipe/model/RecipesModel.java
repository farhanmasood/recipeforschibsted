package com.android.recipe.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 11/14/2017.
 */

public class RecipesModel {

    @SerializedName("recipe_id")
    private String recipeId;

    @SerializedName("title")
    private String title;

    @SerializedName("image_url")
    private String imageUrl;

    public RecipesModel() {
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
