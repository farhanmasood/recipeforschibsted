package com.android.recipe.api;

import com.android.recipe.model.RecipeDetailResponseModel;
import com.android.recipe.model.RecipesResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Farhan on 11/14/2017.
 */

public interface ApiInterface {

    @GET("/api/search")
    Call<RecipesResponseModel> getRecipes(@Query("key") String key, @Query("q") String searchString, @Query("page") int pageNumber);

    @GET("/api/get")
    Call<RecipeDetailResponseModel> getRecipe(@Query("key") String key, @Query("rId") String recipeId);

}
